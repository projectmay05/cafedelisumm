package com.delisumm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String home()
	{
		
		return "index";
	}
	
	@RequestMapping("/home")
	public String homere()
	{
		
		return"redirect:/";
	}

	@RequestMapping("/login")
	public String login()
	{
		
		return"login";
	}
	
}
