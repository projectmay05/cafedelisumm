package com.delisumm.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.delisumm.model.User;
import com.delisumm.services.UserServices;

@Controller
public class UserController {
	
@Autowired 
private UserServices userServices;
 
@RequestMapping("/adduser")
public String addUser(Model model)
{
	User user = new User();
	user.setName("pradeep");
	user.setUsername("paddy");
	user.setPassword("pradeep");
	user.setEmail("pradeep.shinde@gmail.com");
	user.setMobile("55");

	userServices.addUser(user);
	
	return "adduser";
}
 

@RequestMapping(value = "/getRegistration", method = RequestMethod.POST)
public @ResponseBody String getRegistration(@RequestBody String registrationData, Model model) {
	
	System.out.println("controller");
	System.out.println(registrationData);
	//create Jsobj 
	  JSONObject registrationObj = new JSONObject(registrationData);
	  
	 //Prepare Jsobj to sendv response 
	  JSONObject redirectJSON =new JSONObject();
	  
	  String name= registrationObj.getString("name");
	  String username = registrationObj.getString("username");
	  String password = registrationObj.getString("password");
	  String email = registrationObj.getString("email");
	  String mobile = registrationObj.getString("mobile");

	  System.out.println(name);

 
	  redirectJSON.put("name", name);
	  redirectJSON.put("username", username);
	  redirectJSON.put("password", password);
	  redirectJSON.put("email", email);
	  redirectJSON.put("mobile", mobile);


	return redirectJSON.toString();

}

}
