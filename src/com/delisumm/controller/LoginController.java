package com.delisumm.controller;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	//************getLogin**************
	@RequestMapping(value = "/getlogin", method = RequestMethod.POST)
	public @ResponseBody String login(@RequestBody String userdata, Model model) {
		
		System.out.println(userdata);
		
		//create Jsobj 
		  JSONObject userDataObj = new JSONObject(userdata);
		  
		 //Prepare Jsobj to sendv response 
		  JSONObject redirectJSON =new JSONObject();
		  
		  
		  String username = userDataObj.getString("username");
		  
		
		  System.out.println("username" + username); 
		  redirectJSON.put("username", username);
	 
		
		return redirectJSON.toString();

	}

	// logout

	// /registration
	
	@RequestMapping("/registration")
	public String registrationForm()
	{
		return "registration";
	}



}
