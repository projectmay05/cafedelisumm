package com.delisumm.dao;

import java.util.List;

import com.delisumm.model.User;


public interface UserDao {
	
	//Add User
	void addUser(User user);
	
	//Get User By Id
	User getUserById(int id);
	
	//Getll User
	List<User>  getAllUser();
	
	//delete user by id
	void deleteUser(int id);
	
	// Update user 
	
	void UpdateUser(User user);

}




