package com.delisumm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.delisumm.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private	SessionFactory sesionFactory;

	
	@Override
	public void addUser(User user) {
		Session session = sesionFactory.getCurrentSession();
		session.saveOrUpdate(user);
        session.flush();

	
	}

	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void UpdateUser(User User) {
		// TODO Auto-generated method stub
		
	}
	

}
