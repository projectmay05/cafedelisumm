package com.delisumm.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.delisumm.dao.UserDao;
import com.delisumm.model.User;

@Service
public class UserServicesImpl implements UserServices {

	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional
	public void addUser(User user) {
		userDao.addUser(user);
	
	}

}
