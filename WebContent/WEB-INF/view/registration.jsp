<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="/WEB-INF/view/template/header.jsp" %>

 <link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
 <link href="<c:url value="/resources/fonts/ionicons.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/controller.js" />"></script>
 
 <br> <br>
 
 <div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
          
			<div class="card-body">
				<form>
				<!-- Name -->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="glyphicon-class"></i></span>
						</div>
						<input type="text" class="form-control" id="name" placeholder="Please Enter Name">
					</div>
					
				<!-- Username -->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="glyphicon-class"></i></span>
						</div>
						<input type="text" class="form-control" id="username" placeholder="Username">
					</div>
					
					
					<!-- Password -->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" id="password" placeholder="Password">
					</div>
					
					
					<!--Eamil  -->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="text" class="form-control" id="email" placeholder="Email ">
					</div>
				
					
					<!--Mobile -->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="text" class="form-control" id="mobile" placeholder="Mobile ">
					</div>
					
					<div class="form-group">
						<input type="submit" value="Sign Up" class="btn float-right login_btn" onclick=registration();>
					</div>
					<div class="card-header">
				<h3>Delisumm</h3>
				<div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div>
			</div>	
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="registration" >Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div>
			</div>
		</div>
	</div>
</div>


<%@include file="/WEB-INF/view/template/footer.jsp" %>